package by.germanovich.travel_packages.components.agency;

import by.germanovich.travel_packages.components.order.Order;
import by.germanovich.travel_packages.components.customer.Customer;
import by.germanovich.travel_packages.components.tour.Tour;

import java.util.*;

public abstract class Agency {

    private String name;
    private List<Tour> tour;
    private List<Customer> customer;
    private List<Order> order;

    public Agency(String name) {
        this.name = name;
        tour = new ArrayList<>();
        customer = new ArrayList<>();
        order = new ArrayList<>();
    }

    public Agency() {
        tour = new ArrayList<>();
        customer = new ArrayList<>();
        order = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Tour> getTour() {
        return tour;
    }

    public List<Customer> getCustomer() {
        return customer;
    }

    public List<Order> getOrder() {
        return order;
    }

    public void setTour(List<Tour> tour) {
        this.tour = tour;
    }

    public void setCustomer(List<Customer> customer) {
        this.customer = customer;
    }

    public void setOrder(List<Order> order) {
        this.order = order;
    }

    public boolean addOrder(Order order) {
        return this.order.add(order);
    }

    public boolean addTour(Tour tour) {
        return this.tour.add(tour);
    }

    public boolean addClient(Customer customer) {
        return this.customer.add(customer);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Agency agency = (Agency) o;
        return name.equals(agency.name) &&
                tour.equals(agency.tour) &&
                customer.equals(agency.customer) &&
                order.equals(agency.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, tour, customer, order);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Agency.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("tour=" + tour)
                .add("customer=" + customer)
                .add("order=" + order)
                .toString();
    }
}
