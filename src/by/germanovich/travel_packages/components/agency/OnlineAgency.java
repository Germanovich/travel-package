package by.germanovich.travel_packages.components.agency;

import java.util.StringJoiner;

public class OnlineAgency extends Agency {

    private String siteName;

    public OnlineAgency(String name, String siteName) {

        super(name);
        this.siteName = siteName;
    }

    public OnlineAgency() {
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", OnlineAgency.class.getSimpleName() + "[", "]")
                .add("siteName='" + siteName + "'")
                .toString();
    }
}
