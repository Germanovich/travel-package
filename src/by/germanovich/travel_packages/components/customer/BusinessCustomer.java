package by.germanovich.travel_packages.components.customer;

import java.util.StringJoiner;

public class BusinessCustomer extends Customer{

    private int businessCardNumber;

    public BusinessCustomer(String name, String surname, int businessCardNumber) {
        super(name, surname);
        this.businessCardNumber = businessCardNumber;
    }
    public BusinessCustomer() {
    }

    public int getCardNumber() {
        return businessCardNumber;
    }

    public void setCardNumber(int businessCardNumber) {
        this.businessCardNumber = businessCardNumber;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BusinessCustomer.class.getSimpleName() + "[", "]")
                .add(super.getName() + " " + super.getSurname())
                .add("businessCardNumber=" + businessCardNumber)
                .toString();
    }
}
