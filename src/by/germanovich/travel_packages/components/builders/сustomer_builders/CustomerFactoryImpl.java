package by.germanovich.travel_packages.components.builders.сustomer_builders;

import by.germanovich.travel_packages.components.customer.BusinessCustomer;
import by.germanovich.travel_packages.components.customer.ResidentialCustomer;

public class CustomerFactoryImpl implements CustomerFactory {

    public BusinessCustomer createBusinessCustomer() {
        return new BusinessCustomer();
    }

    public ResidentialCustomer createResidentialCustomer() {
        return new ResidentialCustomer();
    }

}
