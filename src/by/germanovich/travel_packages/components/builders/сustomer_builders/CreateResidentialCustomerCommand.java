package by.germanovich.travel_packages.components.builders.сustomer_builders;

import by.germanovich.travel_packages.components.builders.CommandParams;
import by.germanovich.travel_packages.components.customer.Customer;
import by.germanovich.travel_packages.components.customer.ResidentialCustomer;

public class CreateResidentialCustomerCommand  extends AbstractCreateCustomerCommand{
    @Override
    protected ResidentialCustomer createNewCustomer(CommandParams params){
        return customerFactory.createResidentialCustomer();
    }

    protected void fillParams(Customer customer, CommandParams params){
        params.addCard();
        ResidentialCustomer residentialCustomer = ((ResidentialCustomer)customer);
        residentialCustomer.setCardNumber(params.getCard());

        //..other logic

        //throw new CreateResidentialCustomerExeption();
    }
}
