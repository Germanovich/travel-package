package by.germanovich.travel_packages.components.builders.сustomer_builders;

import by.germanovich.travel_packages.components.builders.CommandParams;
import by.germanovich.travel_packages.components.customer.BusinessCustomer;
import by.germanovich.travel_packages.components.customer.Customer;

public class CreateBusinessCustomerCommand extends AbstractCreateCustomerCommand{
    @Override
    protected BusinessCustomer createNewCustomer(CommandParams params){
        return customerFactory.createBusinessCustomer();
    }

    @Override
    protected void fillParams(Customer customer,CommandParams params){
        params.addCard();
        BusinessCustomer businessCustomer = ((BusinessCustomer)customer);
        businessCustomer.setCardNumber(params.getCard());

        //..other logic
        //throw new CreateBusinessCustomerExeption();
    }
}
