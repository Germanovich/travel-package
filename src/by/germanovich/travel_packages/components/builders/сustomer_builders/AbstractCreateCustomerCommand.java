package by.germanovich.travel_packages.components.builders.сustomer_builders;

import by.germanovich.travel_packages.components.builders.Command;
import by.germanovich.travel_packages.components.builders.CommandParams;
import by.germanovich.travel_packages.components.customer.Customer;
import by.germanovich.travel_packages.utils.Collection;

public abstract class AbstractCreateCustomerCommand implements Command {

    protected CustomerFactory customerFactory;

    AbstractCreateCustomerCommand() {
        customerFactory = new CustomerFactoryImpl();
    }

    @Override
    public void execute(CommandParams params, Collection collectionData) {
        Customer customer = createNewCustomer(params);
        customer.setName(params.getName());
        customer.setSurname(params.getSurname());

        fillParams(customer, params);
        collectionData.setCustomer(customer);
    }

    protected abstract Customer createNewCustomer(CommandParams params);

    protected abstract void fillParams(Customer customer, CommandParams params);
}
