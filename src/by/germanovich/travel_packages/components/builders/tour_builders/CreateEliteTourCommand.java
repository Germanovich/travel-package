package by.germanovich.travel_packages.components.builders.tour_builders;

import by.germanovich.travel_packages.components.builders.CommandParams;
import by.germanovich.travel_packages.components.tour.EliteTour;
import by.germanovich.travel_packages.components.tour.Tour;

public class CreateEliteTourCommand extends AbstractCreateTourCommand {
    @Override
    protected EliteTour createNewTour(CommandParams params){
        return tourFactory.createEliteTour();
    }

    @Override
    protected void fillParams(Tour tour, CommandParams params){

        params.addNameHotel();
        params.addCostHotel();

        EliteTour eliteTour = ((EliteTour)tour);

        eliteTour.setNameHotel(params.getNameHotel());
        eliteTour.setCostHotel(params.getCostHotel());
    }
}
