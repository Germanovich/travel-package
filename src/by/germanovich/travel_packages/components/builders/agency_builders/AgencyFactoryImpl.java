package by.germanovich.travel_packages.components.builders.agency_builders;

import by.germanovich.travel_packages.components.agency.LocalAgency;
import by.germanovich.travel_packages.components.agency.OnlineAgency;

public class AgencyFactoryImpl implements AgencyFactory {

    public LocalAgency createLocalAgency() {
        return new LocalAgency();
    }

    public OnlineAgency createOnlineAgency() {
        return new OnlineAgency();
    }
}
