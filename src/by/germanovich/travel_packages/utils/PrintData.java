package by.germanovich.travel_packages.utils;

public class PrintData {
    public static void printCollection(Collection myCollection) {

        if(myCollection.getAgency().size() != 0) {

            System.out.println(Constants.THIN_LINE);


            System.out.println("Агенства:");
            for (int i = 0; i < myCollection.getAgency().size(); i++) {
                System.out.print((i + 1) + ". ");
                System.out.println(myCollection.getAgency().get(i).getName());

                System.out.println("Туры:");
                for (int j = 0; j < myCollection.getAgency().get(i).getTour().size(); j++) {
                    System.out.print((j + 1) + ") ");
                    System.out.print(myCollection.getAgency().get(i).getTour().get(j) + "\n");
                }

                System.out.println("Посетитили:");
                for (int j = 0; j < myCollection.getAgency().get(i).getCustomer().size(); j++) {
                    System.out.print((j + 1) + ") ");
                    System.out.print(myCollection.getAgency().get(i).getCustomer().get(j) + "\n");
                }

                System.out.println("Заказы:");
                for (int j = 0; j < myCollection.getAgency().get(i).getOrder().size(); j++) {
                    System.out.print((j + 1) + ") ");
                    System.out.print(myCollection.getAgency().get(i).getOrder().get(j) + "\n");
                }
                System.out.println(Constants.THIN_LINE);
            }
        }else{
            System.out.println(Constants.THIN_LINE);
            System.out.println("Нет агенств");
            System.out.println(Constants.THIN_LINE);
        }
        if(myCollection.getCustomer().size() != 0) {
            System.out.println("Пользователи:");
            PrintData.printCustomer(myCollection);
        }else{
            System.out.println(Constants.THIN_LINE);
            System.out.println("Нет пользователей");
            System.out.println(Constants.THIN_LINE);
        }
    }

    public static void printAgency(Collection myCollection) {
        if(myCollection.getAgency().size() != 0) {
            System.out.println(Constants.THIN_LINE);

            for (int i = 0; i < myCollection.getAgency().size(); i++) {
                System.out.print((i + 1) + ". ");
                System.out.println(myCollection.getAgency().get(i).getName());
            }

            System.out.println(Constants.THIN_LINE);
        }
        else{
            System.out.println(Constants.THIN_LINE);
            System.out.println("Нет агенств");
            System.out.println(Constants.THIN_LINE);
        }
    }

    public static void printCustomer(Collection myCollection) {
        if(myCollection.getCustomer().size() != 0) {
            System.out.println(Constants.THIN_LINE);

            for (int i = 0; i < myCollection.getCustomer().size(); i++) {
                System.out.print((i + 1) + ". ");
                System.out.print(myCollection.getCustomer().get(i).getName() + " ");
                System.out.print(myCollection.getCustomer().get(i).getSurname() + "");
                System.out.print(" (" + myCollection.getCustomer().get(i).getClass().getSimpleName() + ")\n");
            }

            System.out.println(Constants.THIN_LINE);
        }
        else{
            System.out.println(Constants.THIN_LINE);
            System.out.println("Нет пользователей");
            System.out.println(Constants.THIN_LINE);
        }
    }

    public static void printTour(Collection myCollection, int numberAgency) {
        if(myCollection.getAgency().get(numberAgency).getTour().size() != 0) {
            System.out.println(Constants.THIN_LINE);

            for (int j = 0; j < myCollection.getAgency().get(numberAgency).getTour().size(); j++) {

                System.out.print((j + 1) + ". ");
                System.out.print(myCollection.getAgency().get(numberAgency).getTour().get(j) + "\n");
            }

            System.out.println(Constants.THIN_LINE);
        } else{
            System.out.println(Constants.THIN_LINE);
            System.out.println("В этом агенстве нет туров");
            System.out.println(Constants.THIN_LINE);
        }
    }

    public static void printOrder(Collection myCollection, int numberAgency) {
        if(myCollection.getAgency().get(numberAgency).getOrder().size() != 0) {
            System.out.println(Constants.THIN_LINE);

            for (int j = 0; j < myCollection.getAgency().get(numberAgency).getOrder().size(); j++) {

                System.out.print((j + 1) + ". ");
                System.out.print(myCollection.getAgency().get(numberAgency).getOrder().get(j) + "\n");
            }

            System.out.println(Constants.THIN_LINE);
        } else{
            System.out.println(Constants.THIN_LINE);
            System.out.println("У этого агенства нет заказов");
            System.out.println(Constants.THIN_LINE);
        }
    }
}
