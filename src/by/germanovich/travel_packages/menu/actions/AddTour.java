package by.germanovich.travel_packages.menu.actions;

import by.germanovich.travel_packages.enums.variety.Food;
import by.germanovich.travel_packages.enums.variety.Purpose;
import by.germanovich.travel_packages.enums.variety.Transport;
import by.germanovich.travel_packages.utils.*;

public class AddTour {

    private String country;
    private String name;
    private Purpose purpose;
    private Transport transport;
    private Food food;
    private int duration;
    private int price;

    public AddTour() {

        country = GetData.addNameCountry();
        name = GetData.addNameTour();

        purpose = GetData.addPurpose();
        transport = GetData.addTransport();
        food = GetData.addFood();

        duration = GetData.addDuration();
        price = GetData.addPrice();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Purpose getPurpose() {
        return purpose;
    }

    public void setPurpose(Purpose purpose) {
        this.purpose = purpose;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}

