package by.germanovich.travel_packages.menu.actions;
import by.germanovich.travel_packages.utils.Keyboard;

public class AddCustomer {

    private static String name;
    private static String surname;

    public  AddCustomer() {

        System.out.print("Введите имя: ");
        name = Keyboard.getName();

        System.out.print("Введите фамилию: ");
        surname = Keyboard.getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
