package by.germanovich.travel_packages.enums.variety;

public enum Purpose {
    RECREATION, EXCURSION, TREATMENT, SHOPPING
}
